#!/bin/sh
set -eux

: ${1?"Usage: $0 TESTSDIR"}

TESTSDIR="$1"
SOURCEDIR="${TESTSDIR}/source/"
PACKAGE=parted

cd "${TESTSDIR}"
if [ ! -e ${PACKAGE}.spec ]; then
    echo "Missing ${PACKAGE}.spec"
    pwd
    ls
    exit 1
fi

# This runs from the ./tests/ directory
# Install the dependencies from the spec which MUST be copied over by tests.yml
dnf -y build-dep ${PACKAGE}.spec

# Flattened sources from standard-role-sources
cd "${SOURCEDIR}" || exit

# Rebuild the package in place, also runs the %check
# skip %prep, it was already run on the source before it was copied over
rpmbuild --noprep --nodeps -bb --build-in-place "${TESTSDIR}/${PACKAGE}.spec"
RET=$?
exit ${RET}
